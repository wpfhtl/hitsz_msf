[Github Wiki](https://github.com/ethz-asl/ethzasl_msf/wiki)

## Install

```bash
cd ~/catkin_ws/src/
git clone git@bitbucket.org:HITSZ-NRSL/hitsz_msf.git
git clone git@github.com:ethz-asl/glog_catkin.git
cd ../
catkin_make
```

## Usage

- need imu topic and vrpn position topic (As a rule of thumb you should always excite at least two axes in acceleration and 2 axis in angular rates, to make the data estimate converge)

```bash
roslaunch mavros px4.launch
roslaunch vrpn_client_ros sample.launch
```

- launch msf kalman estimate 

```bash
roslaunch msf_updates viconpos_sensor.launch
```

- The result velocity(ekf estiamte calculate output) topic publish at `/msf_core/velocity`

```yaml
header: 
  seq: 6513
  stamp: 
    secs: 1481772282
    nsecs: 166815758
  frame_id: ''
vector: 
  x: 0.512133665683
  y: 0.788934196864
  z: -0.694052972467
```
`vector/x` is x velocity , `vector/y` is y velocity ,`vector/z` is z velocity 

- The vrpn velocity(calculate by take 10 times the average) is publish in `/vrpn/velocity`, it is like

```yaml
header: 
  seq: 10
  stamp: 
    secs: 1481374403
    nsecs: 939115031
  frame_id: ''
twist: 
  linear: 
    x: 0.207635425414
    y: -0.000926263929153
    z: 0.193587812033
  angular: 
    x: 0.0
    y: 0.0
    z: 0.0
```

`twist/linear/x` is x velocity , `twist/linear/y` is y velocity, `twist/linear/z` is z velocity

- you can see msf velocity estimate plot VS vrpn estimate velocity plot 

```bash
rosrun msf_core plot_velocity
``` 